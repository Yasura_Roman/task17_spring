package com.yasiuraroman.part2.controller;

public interface Controller {
    String getFirstConfig();

    String getSecondConfig();

    String getBeansConfig();
}
