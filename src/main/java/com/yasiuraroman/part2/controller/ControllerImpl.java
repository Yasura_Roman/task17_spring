package com.yasiuraroman.part2.controller;

import com.yasiuraroman.part2.config.BeansConfig;
import com.yasiuraroman.part2.config.FirsConfig;
import com.yasiuraroman.part2.config.SecondConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ControllerImpl implements Controller {
    @Override
    public String getFirstConfig() {
        return getAllBeansOfConfig(FirsConfig.class);
    }

    @Override
    public String getSecondConfig() {
        return getAllBeansOfConfig(SecondConfig.class);
    }

    @Override
    public String getBeansConfig() {
        return getAllBeansOfConfig(BeansConfig.class);
    }

    private String getAllBeansOfConfig(Class clazz) {
        ApplicationContext context = new AnnotationConfigApplicationContext(clazz);
        StringBuilder sb = new StringBuilder();
        for (String beanName: context.getBeanDefinitionNames()) {
            sb.append(beanName + "\n");
        }
        return sb.toString();
    }
}
