package com.yasiuraroman.part2.view;

@FunctionalInterface
public interface Printable {
    void print();
}
