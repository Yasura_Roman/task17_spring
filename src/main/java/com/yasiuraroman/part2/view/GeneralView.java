package com.yasiuraroman.part2.view;

import com.yasiuraroman.part2.controller.Controller;
import com.yasiuraroman.part2.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GeneralView {
    private static Logger logger = LogManager.getLogger();
    private static Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Controller controller;

    public GeneralView() {
        controller = new ControllerImpl();
        menu = new HashMap<>();
        methodsMenu = new HashMap<>();

        menu.put("1"," 1 - get first configuration ");
        menu.put("2"," 2 - get second configuration ");
        menu.put("3"," 3 - get @beans configuration ");
        menu.put("Q"," Q - exit");
        methodsMenu.put("1", this::getFirstConfig);
        methodsMenu.put("2", this::getSecondConfig);
        methodsMenu.put("3", this::getBeansConfig);

    }

    private void getFirstConfig(){
        logger.info(controller.getFirstConfig());
    }
    private void  getSecondConfig(){
        logger.info(controller.getSecondConfig());
    }
    private void  getBeansConfig(){
        logger.info(controller.getBeansConfig());
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
