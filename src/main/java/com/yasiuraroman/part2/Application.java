package com.yasiuraroman.part2;

import com.yasiuraroman.part2.view.GeneralView;

public class Application {
    public static void main(String[] args) {
        GeneralView view = new GeneralView();
        view.show();
    }
}
