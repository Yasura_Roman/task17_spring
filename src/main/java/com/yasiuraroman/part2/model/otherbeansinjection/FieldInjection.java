package com.yasiuraroman.part2.model.otherbeansinjection;

import com.yasiuraroman.part2.model.otherbeans.OtherBeanA;
import com.yasiuraroman.part2.model.otherbeans.OtherBeanB;
import com.yasiuraroman.part2.model.otherbeans.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;

public class FieldInjection {
    @Autowired
    private OtherBeanA beanA;
    @Autowired
    private OtherBeanB beanB;
    @Autowired
    private OtherBeanC beanC;

    public FieldInjection() {
    }

    public FieldInjection(OtherBeanA beanA, OtherBeanB beanB, OtherBeanC beanC) {
        this.beanA = beanA;
        this.beanB = beanB;
        this.beanC = beanC;
    }

    public OtherBeanA getBeanA() {
        return beanA;
    }

    public void setBeanA(OtherBeanA beanA) {
        this.beanA = beanA;
    }

    public OtherBeanB getBeanB() {
        return beanB;
    }

    public void setBeanB(OtherBeanB beanB) {
        this.beanB = beanB;
    }

    public OtherBeanC getBeanC() {
        return beanC;
    }

    public void setBeanC(OtherBeanC beanC) {
        this.beanC = beanC;
    }
}
