package com.yasiuraroman.part2.model.otherbeansinjection;

import com.yasiuraroman.part2.model.otherbeans.OtherBeanA;
import com.yasiuraroman.part2.model.otherbeans.OtherBeanB;
import com.yasiuraroman.part2.model.otherbeans.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;

public class SetterInjection {
    private OtherBeanA beanA;
    private OtherBeanB beanB;
    private OtherBeanC beanC;

    public SetterInjection() {
    }

    public SetterInjection(OtherBeanA beanA, OtherBeanB beanB, OtherBeanC beanC) {
        this.beanA = beanA;
        this.beanB = beanB;
        this.beanC = beanC;
    }

    public OtherBeanA getBeanA() {
        return beanA;
    }

    @Autowired
    public void setBeanA(OtherBeanA beanA) {
        this.beanA = beanA;
    }

    public OtherBeanB getBeanB() {
        return beanB;
    }

    @Autowired
    public void setBeanB(OtherBeanB beanB) {
        this.beanB = beanB;
    }

    public OtherBeanC getBeanC() {
        return beanC;
    }

    @Autowired
    public void setBeanC(OtherBeanC beanC) {
        this.beanC = beanC;
    }
}
