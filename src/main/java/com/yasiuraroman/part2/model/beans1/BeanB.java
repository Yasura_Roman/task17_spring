package com.yasiuraroman.part2.model.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanB {
    private String name;
    private int value;

    public BeanB() {
    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
