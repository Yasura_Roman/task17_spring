package com.yasiuraroman.part2.model.beaninterface;

import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(4)
@Primary
public class BeanIntA implements BeanInterface {
}
