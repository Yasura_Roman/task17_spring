package com.yasiuraroman.part2.model.beaninterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BeanList {
    @Autowired
    private List<BeanInterface> beanInterfaces;
}
