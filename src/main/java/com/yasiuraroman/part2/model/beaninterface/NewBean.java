package com.yasiuraroman.part2.model.beaninterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class NewBean {
    @Autowired
    private BeanInterface beanA;
    @Autowired
    @Qualifier("B")
    private BeanInterface beanB;
    @Autowired
    @Qualifier("C")
    private BeanInterface beanC;
    @Autowired
    @Qualifier("D")
    private BeanInterface beanD;
}
