package com.yasiuraroman.part2.model.beaninterface;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
@Qualifier("B")
public class BeanIntB implements BeanInterface {
}
