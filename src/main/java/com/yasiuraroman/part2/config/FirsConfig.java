package com.yasiuraroman.part2.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.yasiuraroman.part2.model.beans1")
public class FirsConfig {
}
