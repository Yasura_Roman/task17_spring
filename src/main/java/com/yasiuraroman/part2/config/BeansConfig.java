package com.yasiuraroman.part2.config;

import com.yasiuraroman.part2.model.otherbeans.OtherBeanA;
import com.yasiuraroman.part2.model.otherbeans.OtherBeanB;
import com.yasiuraroman.part2.model.otherbeans.OtherBeanC;
import org.springframework.context.annotation.Bean;

public class BeansConfig {
    @Bean
    public OtherBeanA otherBeanA() {
        return new OtherBeanA();
    }
    @Bean
    public OtherBeanB otherBeanB() {
        return new OtherBeanB();
    }
    @Bean
    public OtherBeanC otherBeanC() {
        return new OtherBeanC();
    }
}