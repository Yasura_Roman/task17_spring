package com.yasiuraroman.part2.config;

import com.yasiuraroman.part2.model.beans3.BeanD;
import com.yasiuraroman.part2.model.beans3.BeanF;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "com.yasiuraroman.part2.model.beans2",
        useDefaultFilters = false,
        includeFilters = @ComponentScan.Filter(type = FilterType.REGEX,
                pattern = "com\\.yasiuraroman\\.part2\\.model\\.beans2\\..*Flower"))
@ComponentScan(basePackages = "com.yasiuraroman.part2.model.beans3",
        useDefaultFilters = false,
        includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
                classes = { BeanD.class, BeanF.class}))
public class SecondConfig {
}
