package com.yasiuraroman.part1.model;

public interface BeanValidator {
    boolean validate();
}
