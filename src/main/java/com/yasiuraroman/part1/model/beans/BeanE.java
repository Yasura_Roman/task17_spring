package com.yasiuraroman.part1.model.beans;

import com.yasiuraroman.part1.model.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {
    private static Logger logger = LogManager.getLogger();
    private String name;
    private int value;

    public BeanE() {
    }

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean validate() {
        if ( value > 1000) {
            return false;
        }
        return true;
    }

    @PostConstruct
    public void init() {
        logger.info(this.getClass().getName() + " PostConstruct init");
    }

    @PreDestroy
    public void destroy() {
        logger.info(this.getClass().getName() + " PreDestroy");
    }
}
