package com.yasiuraroman.part1.model.beans;

import com.yasiuraroman.part1.model.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanB implements BeanValidator {
    private static Logger logger = LogManager.getLogger();
    private String name;
    private int value;

    public BeanB() {
    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean validate() {
        if (name.length() < 5 || value < 0) {
            return false;
        }
        return true;
    }

    public void init() {
        logger.info(this.getClass().getName() + " init");
    }

    public void destroy() {
        logger.info(this.getClass().getName() + " destroy");
    }

    public void alternativeInit() {
        logger.info(this.getClass().getName() + " alternative init");
    }
}
