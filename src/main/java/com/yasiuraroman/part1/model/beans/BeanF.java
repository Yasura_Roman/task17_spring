package com.yasiuraroman.part1.model.beans;

import com.yasiuraroman.part1.model.BeanValidator;

public class BeanF implements BeanValidator {
    private String name;
    private int value;

    public BeanF() {
    }

    public BeanF(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean validate() {
        if (name.length() < 3 || value >1000) {
            return false;
        }
        return true;
    }
}
