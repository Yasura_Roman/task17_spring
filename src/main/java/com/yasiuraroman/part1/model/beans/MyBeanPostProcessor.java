package com.yasiuraroman.part1.model.beans;

import com.yasiuraroman.part1.model.BeanValidator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof BeanValidator) {
            ((BeanValidator) bean).validate();
        }
        if (bean instanceof BeanA) {
            if (((BeanA) bean).getName().isEmpty() || ((BeanA) bean).getValue()<0){
                throw new ByBeansException(beanName) ;
            }
        }
        if (bean instanceof BeanB) {
            if (((BeanB) bean).getName().isEmpty() || ((BeanB) bean).getValue()<0){
                throw new ByBeansException(beanName) ;
            }
        }
        if (bean instanceof BeanC) {
            if (((BeanC) bean).getName().isEmpty() || ((BeanC) bean).getValue()<0){
                throw new ByBeansException(beanName) ;
            }
        }
        if (bean instanceof BeanD) {
            if (((BeanD) bean).getName().isEmpty() || ((BeanD) bean).getValue()<0){
                throw new ByBeansException(beanName) ;
            }
        }
        if (bean instanceof BeanE) {
            if (((BeanE) bean).getName().isEmpty() || ((BeanE) bean).getValue()<0){
                throw new ByBeansException(beanName) ;
            }
        }
        if (bean instanceof BeanF) {
            if (((BeanF) bean).getName().isEmpty() || ((BeanF) bean).getValue()<0){
                throw new ByBeansException(beanName) ;
            }
        }
        return bean;
    }
}

class ByBeansException extends BeansException{

    public ByBeansException(String msg) {
        super(msg);
    }
}
