package com.yasiuraroman.part1.model.beans;

import com.yasiuraroman.part1.model.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private static Logger logger = LogManager.getLogger();
    private String name;
    private int value;

    public BeanA() {
    }

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean validate() {
        if (name.length() < 3 || value == 0) {
            return false;
        }
        return true;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info(this.getClass().getName() + "afterPropertiesSet from InitializingBean");
    }

    @Override
    public void destroy() throws Exception {
        logger.info(this.getClass().getName() + "destroy from DisposableBean");
    }

}
