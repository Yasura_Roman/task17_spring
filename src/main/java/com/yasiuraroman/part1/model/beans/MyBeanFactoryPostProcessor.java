package com.yasiuraroman.part1.model.beans;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(
            ConfigurableListableBeanFactory configurableLBF) throws BeansException {
        for (String beanName: configurableLBF.getBeanDefinitionNames()) {
            if (beanName.equals("beanB")) {
                BeanDefinition beanDefinition = configurableLBF.getBeanDefinition(beanName);
                beanDefinition.setInitMethodName("alternativeInit");
            }
        }
    }
}
