package com.yasiuraroman.part1.view;

@FunctionalInterface
public interface Printable {
    void print();
}
