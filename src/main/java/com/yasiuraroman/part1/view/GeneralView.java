package com.yasiuraroman.part1.view;

import com.yasiuraroman.part1.controller.Controller;
import com.yasiuraroman.part1.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GeneralView {
    private static Logger logger = LogManager.getLogger();
    private static Scanner input = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Controller controller;

    public GeneralView() {
        controller = new ControllerImpl();
        menu = new HashMap<>();
        methodsMenu = new HashMap<>();

        menu.put("1"," 1 - get Configuration of all Beans");
        menu.put("2"," 2 - get all Beans created by GeneralConfig");
        menu.put("Q"," Q - exit");
        methodsMenu.put("1", this::getConfigurationOfAllBeans);
        methodsMenu.put("2", this::getAllBeansCreatedByGeneralConfig);

    }

    private void getConfigurationOfAllBeans(){
        logger.info(controller.getConfigurationOfAllBeans());
    }
    private void  getAllBeansCreatedByGeneralConfig(){
        logger.info(controller.getAllBeansCreatedByGeneralConfig());
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
