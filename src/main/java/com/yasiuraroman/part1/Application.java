package com.yasiuraroman.part1;

import com.yasiuraroman.part1.view.GeneralView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger logger = LogManager.getLogger();
    public static void main(String[] args) {
        GeneralView view = new GeneralView();
        view.show();
    }
}
