package com.yasiuraroman.part1.controller;

import com.yasiuraroman.part1.config.GeneralConfig;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ControllerImpl implements Controller {

    public String getConfigurationOfAllBeans() {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(GeneralConfig.class);
        StringBuilder stringBuilder = new StringBuilder();
        for(String name: context.getBeanDefinitionNames()) {
            BeanDefinition beanDefinition = context.getBeanDefinition(name);
            stringBuilder.append(beanDefinition + " \n");
        }
        return stringBuilder.toString();
    }

    public String getAllBeansCreatedByGeneralConfig() {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(GeneralConfig.class);
        StringBuilder stringBuilder = new StringBuilder();
        for (String beanDefinitionName : context.getBeanDefinitionNames()) {
            stringBuilder.append(beanDefinitionName + " \n");
        }
        return stringBuilder.toString();
    }
}
