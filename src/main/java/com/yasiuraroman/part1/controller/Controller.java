package com.yasiuraroman.part1.controller;

public interface Controller {
    String getConfigurationOfAllBeans();
    String getAllBeansCreatedByGeneralConfig();
}
