package com.yasiuraroman.part1.config;

import com.yasiuraroman.part1.model.beans.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;

@Configuration
@PropertySource("value.properties")
public class FirsConfig {
    @Value("${beanB.name}")
    private String beanBName;
    @Value("${beanB.value}")
    private int beanBValue;
    @Value("${beanC.name}")
    private String beanCName;
    @Value("${beanC.value}")
    private int beanCValue;
    @Value("${beanD.name}")
    private String beanDName;
    @Value("${beanD.value}")
    private int beanDValue;

    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Order(2)
    public BeanB getBeanB(){
        return new BeanB(beanBName, beanBValue);
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Order(3)
    public BeanC getBeanC(){
        return new BeanC(beanCName, beanCValue);
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Order(1)
    public BeanD getBeanD(){
        return new BeanD(beanDName, beanDValue);
    }

    @Bean("beanAFirst")
    public BeanA getBeanAFirst(BeanB beanB, BeanC beanC) {
        return new BeanA(beanB.getName(),beanC.getValue());
    }

    @Bean("beanASecond")
    public BeanA getBeanASecond(BeanB beanB, BeanD beanD) {
        return new BeanA(beanB.getName(),beanD.getValue());
    }

    @Qualifier("AThird")
    @Bean("beanAThird")
    public BeanA getBeanAThird(BeanD beanD, BeanC beanC) {
        return new BeanA(beanD.getName(),beanC.getValue());
    }


    @Bean BeanA getBeanA(){
        return new BeanA();
    }

    @Bean("beanEFirst")
    public BeanE getBeanEFirst(BeanA beanAFirst) {
        return new BeanE(beanAFirst.getName(), beanAFirst.getValue());
    }

    @Bean("beanESecond")
    public BeanE getBeanESecond(@Qualifier("beanASecond") BeanA beanA) {
        return new BeanE(beanA.getName(), beanA.getValue());
    }

    @Bean("beanEThird")
    public BeanE getBeanEThird(@Qualifier("AThird") BeanA beanA) {
        return new BeanE(beanA.getName(), beanA.getValue());
    }

    @Bean
    @Lazy
    public BeanF getBeanF() {
        return new BeanF("beanF", 6);
    }
}
