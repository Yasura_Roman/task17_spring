package com.yasiuraroman.part1.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(FirsConfig.class)
public class GeneralConfig  {
}
